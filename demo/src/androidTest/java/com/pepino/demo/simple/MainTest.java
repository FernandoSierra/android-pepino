package com.pepino.demo.simple;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;

import com.pepino.TestFeature;
import com.pepino.annotations.And;
import com.pepino.annotations.FeatureOptions;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;
import com.pepino.demo.MainActivity;
import com.pepino.demo.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/11/16
 */
@FeatureOptions(feature = "home.feature")
public class MainTest extends TestFeature<MainActivity> {

    @Override
    public ActivityTestRule<MainActivity> getActivityTestRule() {
        return new ActivityTestRule<>(MainActivity.class);
    }

    @Given("Empty field")
    public void cleanNameField() {
        onView(ViewMatchers.withId(R.id.edit_name))
                .perform(clearText());
    }

    @When("Enter a <name>")
    public void writeName(String name) {
        onView(withId(R.id.edit_name))
                .perform(typeText(name), closeSoftKeyboard());
    }

    @When("Press ok button")
    @And({"Press ok button", "Press ok button again"})
    public void pressOkButton() {
        onView(withId(R.id.button_ok))
                .perform(click());
    }

    @Then("Message is displayed with <name>")
    public void checkSnackMessage(String name) {
        Context context = InstrumentationRegistry.getTargetContext();
        String message = context.getString(R.string.message_main_snack) + name;
        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(message)))
                .check(matches(isDisplayed()));
    }

    @Then("Error is displayed")
    public void checkSnackError() {
        onView(allOf(withId(android.support.design.R.id.snackbar_text),
                withText(R.string.error_main_snack)))
                .check(matches(isDisplayed()));
    }
}
