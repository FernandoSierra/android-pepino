package com.pepino.demo.complete;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;

import com.pepino.TestFeature;
import com.pepino.annotations.And;
import com.pepino.annotations.FeatureOptions;
import com.pepino.annotations.Given;
import com.pepino.annotations.When;
import com.pepino.demo.MainActivity;
import com.pepino.demo.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/11/16
 */
@FeatureOptions(feature = "home.feature", classes = {CommonSteps.class})
public class MainTest2 extends TestFeature<MainActivity> {

    @Override
    public ActivityTestRule<MainActivity> getActivityTestRule() {
        return new ActivityTestRule<>(MainActivity.class);
    }

    @Given("Empty field")
    public void cleanNameField() {
        onView(ViewMatchers.withId(R.id.edit_name))
                .perform(clearText());
    }

    @When("Enter a <name>")
    public void writeName(String name) {
        onView(withId(R.id.edit_name))
                .perform(typeText(name), closeSoftKeyboard());
    }

    @When("Press ok button")
    @And({"Press ok button", "Press ok button again"})
    public void pressOkButton() {
        onView(withId(R.id.button_ok))
                .perform(click());
    }
}
