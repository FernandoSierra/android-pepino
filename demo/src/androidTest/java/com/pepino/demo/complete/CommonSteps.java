package com.pepino.demo.complete;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.pepino.annotations.Then;
import com.pepino.demo.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 14/11/16
 */
public class CommonSteps {

    @Then("Message is displayed with <name>")
    public void checkSnackMessage(String name) {
        Context context = InstrumentationRegistry.getTargetContext();
        String message = context.getString(R.string.message_main_snack) + name;
        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(message)))
                .check(matches(isDisplayed()));
    }

    @Then("Error is displayed")
    public void checkSnackError() {
        onView(allOf(withId(android.support.design.R.id.snackbar_text),
                withText(R.string.error_main_snack)))
                .check(matches(isDisplayed()));
    }
}
