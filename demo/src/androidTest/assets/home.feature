Feature: Test home screen

  Scenario Outline: Happy path
    Given Empty field
    When Enter a <name>
    And Press ok button
    Then Message is displayed with <name>
    Examples:
      | name     |
      | Fernando |
      | Sierra   |

    Examples:
      | name    |
      | Adriana |
      | Robles  |

  Scenario: Error path
    Given Empty field
    When Press ok button
    And Press ok button again
    Then Error is displayed