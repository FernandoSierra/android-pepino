package com.pepino.demo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 6/10/16
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout container;
    private EditText editText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        container = (LinearLayout) findViewById(R.id.linear_main_container);
        editText = ((EditText) findViewById(R.id.edit_name));
        findViewById(R.id.button_ok).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String name = editText.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            Snackbar.make(container, R.string.error_main_snack, Snackbar.LENGTH_LONG).show();
        } else {
            Snackbar.make(container, getString(R.string.message_main_snack) + name,
                    Snackbar.LENGTH_LONG).show();
        }
    }
}
