package com.pepino;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.pepino.annotations.processor.Annotation;
import com.pepino.annotations.processor.AnnotationsWrapper;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gherkin.ast.ScenarioDefinition;
import gherkin.ast.ScenarioOutline;
import gherkin.ast.Step;
import gherkin.ast.TableCell;
import gherkin.ast.TableRow;

/**
 * We should extend from this class for each feature file. You need to annotate this class with
 * {@link com.pepino.annotations.FeatureOptions}. By the default we can write the
 * step's definition in this class. The {@link com.pepino.runner.FeatureTestRunner}
 * will inject the static fields via reflection in Runtime.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @author Javier Marsicano
 * @version 1.0
 * @since 6/10/16
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public abstract class TestFeature<T extends Activity> {
    /**
     * Scenario to be executed.
     */
    public static ScenarioDefinition sScenario;
    /**
     * It contains all the information necessary to execute the scenario
     */
    public static AnnotationsWrapper sWrapper;
    /**
     * It controls the instances required by the test.
     */
    public static Map<Class, Object> sInstancesMap;
    /**
     * It contains the values for the example that be executed.
     */
    public static TableRow sExampleValues;
    /**
     * It contains the names of the values stored in {@link #sExampleValues};
     */
    public static TableRow sExampleNames;
    @Rule
    public ActivityTestRule<T> rule = getActivityTestRule();
    protected Context mTargetContext = InstrumentationRegistry.getTargetContext();

    /**
     * This unique test will be executed by the
     * {@link com.pepino.runner.FeatureTestRunner} one time per Scenario and Example
     * specified by the feature file. DON'T OVERWRITE IT.
     *
     * @throws Exception If something goes wrong, notifies it to the test runner.
     */
    @Test
    public void featureTest() throws Exception {
        if (sScenario == null || sScenario.getSteps() == null) {
            throw new RuntimeException("Invalid feature");
        } else {
            executeFeature();
        }
    }

    private void executeFeature() throws IllegalAccessException, InstantiationException,
            InvocationTargetException {
        for (Step step : sScenario.getSteps()) {
            Annotation annotation = new Annotation(step.getKeyword().trim(), step.getText());
            Method method = sWrapper.getMethodsMap().get(annotation);
            if (method == null) {
                throw new RuntimeException("Step '" + annotation.toString() + "' not defined");
            } else {
                try {
                    executeMethod(method, getMatchingArgs(step.getText()));
                } catch (InvocationTargetException e) {
                    throw new RuntimeException(annotation.toString(), e.getTargetException());
                }
            }
        }
    }

    private void executeMethod(Method method, String[] args) throws IllegalAccessException,
            InstantiationException, InvocationTargetException {
        Class<?> clazz = method.getDeclaringClass();
        Object instance = sInstancesMap.get(clazz);
        if (instance == null) {
            if (clazz.isAssignableFrom(this.getClass())) {
                instance = this;
            } else {
                instance = clazz.newInstance();
            }
        }
        sInstancesMap.put(clazz, instance);
        method.invoke(instance, (Object[]) args);
    }

    private String[] getMatchingArgs(String stepText) {
        ArrayList<String> args = new ArrayList<>();
        if (sExampleNames == null) {
            return null;
        } else {
            Pattern pattern = Pattern.compile("(<(.*?)>)");
            Matcher matcher = pattern.matcher(stepText);
            while (matcher.find()) {
                List<TableCell> headerCells = sExampleNames.getCells();
                String placeholder = matcher.group(1).substring(1, matcher.group(1).length() - 1);
                for (int i = 0; i < headerCells.size(); i++) {
                    /**
                     * If a placeholder name is the same as a column title in the Examples table
                     * then this is the value that will replace it.
                     * */
                    if (placeholder.equals(headerCells.get(i).getValue())) {
                        args.add(sExampleValues.getCells().get(i).getValue());
                        break;
                    }
                }
            }
            String[] out = new String[args.size()];
            return args.toArray(out);
        }
    }

    @CallSuper
    @After
    public void tearDown() throws Exception {
        ActivitiesFinisher.finishOpenActivities();
    }


    @NonNull
    public String getTestDescription() {
        String name = "Scenario: " + sScenario.getName();
        if (sScenario instanceof ScenarioOutline) {
            name += " (" + sExampleValues.getCells().get(0).getValue() + ")";
            //name += getExamplesString();
        }
        return name;
    }

    @Nullable
    private String getExamplesString() {
        String rowString = "{";
        for (int index = 0; index < sExampleNames.getCells().size(); index++) {
            rowString += index > 0 ? ", " : "";
            rowString += sExampleNames.getCells().get(index).getValue() +
                    "='" +
                    sExampleValues.getCells().get(index).getValue() +
                    "'";
        }
        rowString += "}";
        return rowString;
    }

    /**
     * It provides the information of where our test should start.
     *
     * @return A new ActivityTestRule that wraps the Activity specified by {@link T}.
     */
    public abstract ActivityTestRule<T> getActivityTestRule();
}
