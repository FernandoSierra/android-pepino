package com.pepino;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitor;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import android.support.test.runner.lifecycle.Stage;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * Finishes all the open activities during the test.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 7/11/16
 */
final class ActivitiesFinisher implements Runnable {

    private ActivityLifecycleMonitor activityLifecycleMonitor;

    private ActivitiesFinisher() {
        this.activityLifecycleMonitor = ActivityLifecycleMonitorRegistry.getInstance();
    }

    static void finishOpenActivities() {
        new Handler(Looper.getMainLooper()).post(new ActivitiesFinisher());
    }

    @Override
    public void run() {
        final List<Activity> activities = new ArrayList<>();

        for (final Stage stage : EnumSet.range(Stage.CREATED, Stage.STOPPED)) {
            activities.addAll(activityLifecycleMonitor.getActivitiesInStage(stage));
        }

        for (final Activity activity : activities) {
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
    }
}
