package com.pepino.annotations.processor;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Models and wraps the information extracted by the {@link AnnotationsProcessor} from the
 * classes annotated.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 7/10/16
 */
public class AnnotationsWrapper {
    private String mFeature;
    private Class[] mClasses;
    private Map<Annotation, Method> mMethodsMap = new HashMap<>();

    public String getFeature() {
        return mFeature;
    }

    public void setFeature(String feature) {
        mFeature = feature;
    }

    public Map<Annotation, Method> getMethodsMap() {
        return mMethodsMap;
    }

    public Class[] getClasses() {
        return mClasses;
    }

    public void setClasses(Class[] classes) {
        mClasses = classes;
    }
}
