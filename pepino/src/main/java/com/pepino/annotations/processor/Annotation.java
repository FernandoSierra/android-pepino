package com.pepino.annotations.processor;

/**
 * Models the information contained in {@link com.pepino.annotations.Given},
 * {@link com.pepino.annotations.When},
 * {@link com.pepino.annotations.Then},
 * and {@link com.pepino.annotations.And} annotations.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 7/10/16
 */
public class Annotation {
    private String mType;
    private String mValue;

    public Annotation(String type, String value) {
        mType = type;
        mValue = value;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }

    @Override
    public int hashCode() {
        int result = mType.hashCode();
        result = 31 * result + mValue.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Annotation that = (Annotation) o;

        if (!mType.equals(that.mType)) return false;
        return mValue.equals(that.mValue);

    }

    @Override
    public String toString() {
        return mType + " " + mValue;
    }
}
