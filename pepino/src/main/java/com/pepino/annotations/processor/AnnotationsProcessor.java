package com.pepino.annotations.processor;

import android.support.annotation.NonNull;

import com.pepino.annotations.And;
import com.pepino.annotations.FeatureOptions;
import com.pepino.annotations.Given;
import com.pepino.annotations.Then;
import com.pepino.annotations.When;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Class to explore and extract the annotated values.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 6/10/16
 */
public class AnnotationsProcessor {

    private AnnotationsProcessor() {
        // To prevent instantiation.
    }

    /**
     * Reads the {@link FeatureOptions} annotation from the clazz, then it extracts the annotations
     * declared in this clazz and the ones listed by {@link FeatureOptions#classes()}.
     *
     * @param clazz Class to be reviewed.
     * @return A new instance with all the information extracted from it.
     */
    public static AnnotationsWrapper extractAnnotations(@NonNull Class<?> clazz) {
        AnnotationsWrapper wrapper = getFeatureValues(clazz);
        extractAnnotatedMethods(clazz, wrapper);
        for (Class extraClazz : wrapper.getClasses()) {
            extractAnnotatedMethods(extraClazz, wrapper);
        }
        return wrapper;
    }

    private static void extractAnnotatedMethods(@NonNull Class<?> clazz,
                                                @NonNull AnnotationsWrapper wrapper) {
        for (Method method : clazz.getMethods()) {
            List<Annotation> annotations = getAnnotations(method);
            Map<Annotation, Method> methodsMap = wrapper.getMethodsMap();
            for (Annotation annotation : annotations) {
                if (!methodsMap.containsKey(annotation)) {
                    methodsMap.put(annotation, method);
                }
            }
        }
    }

    @NonNull
    private static List<Annotation> getAnnotations(Method method) {
        List<Annotation> annotations = new ArrayList<>();
        if (method.isAnnotationPresent(Given.class)) {
            String[] values = method.getAnnotation(Given.class).value();
            for (String value : values) {
                annotations.add(new Annotation(Given.class.getSimpleName(), value));
            }
        }
        if (method.isAnnotationPresent(When.class)) {
            String[] values = method.getAnnotation(When.class).value();
            for (String value : values) {
                annotations.add(new Annotation(When.class.getSimpleName(), value));
            }
        }
        if (method.isAnnotationPresent(Then.class)) {
            String[] values = method.getAnnotation(Then.class).value();
            for (String value : values) {
                annotations.add(new Annotation(Then.class.getSimpleName(), value));
            }
        }
        if (method.isAnnotationPresent(And.class)) {
            String[] values = method.getAnnotation(And.class).value();
            for (String value : values) {
                annotations.add(new Annotation(And.class.getSimpleName(), value));
            }
        }
        return annotations;
    }

    @NonNull
    private static AnnotationsWrapper getFeatureValues(@NonNull Class<?> clazz) {
        AnnotationsWrapper wrapper = new AnnotationsWrapper();
        FeatureOptions featureOptions = clazz.getAnnotation(FeatureOptions.class);
        wrapper.setFeature(featureOptions.feature());
        wrapper.setClasses(featureOptions.classes());
        return wrapper;
    }
}
