package com.pepino.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to indicate the feature file to be executed, and the external classes where are the
 * step's definitions.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 6/10/16
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface FeatureOptions {
    /**
     * @return Feature file to be executed. It should be under assets folder.
     */
    String feature();

    /**
     * @return Additional classes where are defined the steps. By default, it returns a empty array.
     */
    Class[] classes() default {};
}
