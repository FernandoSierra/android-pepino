package com.pepino.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to indicate the code to be executed when the sentence is used in the feature file.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 6/10/16
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Given {
    String[] value();
}
