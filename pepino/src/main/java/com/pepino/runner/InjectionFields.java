package com.pepino.runner;

import com.pepino.TestFeature;

/**
 * Field names to allow access to {@link TestFeature}'s public static values
 * and inject them using Reflection.
 *
 * @author Javier Marsicano
 * @version 1.0
 * @since 13/10/16.
 */
public class InjectionFields {
    public static final String SCENARIO = "sScenario";
    public static final String WRAPPER = "sWrapper";
    public static final String INSTANCES_MAP = "sInstancesMap";
    public static final String TABLE_ROW = "sExampleValues";
    public static final String TABLE_HEADER = "sExampleNames";
}
