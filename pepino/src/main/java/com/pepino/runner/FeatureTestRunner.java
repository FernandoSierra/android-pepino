package com.pepino.runner;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.test.InstrumentationRegistry;
import android.support.test.internal.runner.RunnerArgs;
import android.support.test.internal.runner.TestExecutor;
import android.support.test.internal.runner.TestRequest;
import android.support.test.internal.runner.TestRequestBuilder;
import android.support.test.internal.runner.listener.ActivityFinisherRunListener;
import android.support.test.internal.runner.listener.CoverageListener;
import android.support.test.internal.runner.listener.DelayInjector;
import android.support.test.internal.runner.listener.InstrumentationResultPrinter;
import android.support.test.internal.runner.listener.LogRunListener;
import android.support.test.internal.runner.listener.SuiteAssignmentPrinter;
import android.support.test.internal.runner.tracker.AnalyticsBasedUsageTracker;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.runner.MonitoringInstrumentation;
import android.support.test.runner.UsageTrackerFacilitator;
import android.support.test.runner.lifecycle.ApplicationLifecycleCallback;
import android.support.test.runner.lifecycle.ApplicationLifecycleMonitorRegistry;
import android.util.Log;

import com.pepino.TestFeature;
import com.pepino.annotations.processor.AnnotationsWrapper;
import com.pepino.runner.internal.FeatureTestRequestBuilder;

import org.junit.runner.Runner;
import org.junit.runner.notification.RunListener;
import org.junit.runners.Suite;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gherkin.AstBuilder;
import gherkin.Parser;
import gherkin.ast.Examples;
import gherkin.ast.Feature;
import gherkin.ast.GherkinDocument;
import gherkin.ast.ScenarioDefinition;
import gherkin.ast.ScenarioOutline;
import gherkin.ast.TableRow;

import static com.pepino.annotations.processor.AnnotationsProcessor.extractAnnotations;

/**
 * Extends and modifies the original {@link android.support.test.runner.AndroidJUnitRunner}.
 * When the test starts, it reads the annotations in the {@link TestFeature}
 * and it will parse the feature file stored in the assets. Then it reads the annotated methods
 * and make the bounds between they and the {@link Feature}. Later it will execute a test for
 * each Scenario and Example's row, if it has.
 * <p>
 * Usage:
 * <p>
 * Replace the {@link android.support.test.runner.AndroidJUnitRunner} in the default config in your
 * project's gradle file for this.
 * <p>
 * <pre>
 * defaultConfig {
 *      testInstrumentationRunner "{@link com.pepino.runner.FeatureTestRunner}"
 *      }
 * </pre>
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @see android.support.test.runner.AndroidJUnitRunner
 * @since 11/10/16
 */
public class FeatureTestRunner extends MonitoringInstrumentation {

    private static final String DEBUG = "debug";
    private static final String CLASS = "class";
    private static final String LOG_TAG = "FeatureTestRunner";
    private static final String RUNNERS = "runners";

    private Bundle mArguments;
    private InstrumentationResultPrinter mInstrumentationResultPrinter = null;
    private RunnerArgs mRunnerArgs;
    private UsageTrackerFacilitator mUsageTrackerFacilitator;
    private AnnotationsWrapper mWrapper;
    private Map<Class, Object> mInstancesMap = new HashMap<>();

    @Override
    public void onCreate(Bundle arguments) {
        mArguments = arguments;
        parseRunnerArgs(mArguments);

        if (mRunnerArgs.debug) {
            Log.i(LOG_TAG, "Waiting for debugger to connect...");
            Debug.waitForDebugger();
            Log.i(LOG_TAG, "Debugger connected.");
        }

        mUsageTrackerFacilitator = new UsageTrackerFacilitator(mRunnerArgs);

        super.onCreate(arguments);

        for (ApplicationLifecycleCallback listener : mRunnerArgs.appListeners) {
            ApplicationLifecycleMonitorRegistry.getInstance().addLifecycleCallback(listener);
        }

        start();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mRunnerArgs.idle) {
            // TODO this is just proof of concept. A more detailed implementation will follow
            Log.i(LOG_TAG, "Runner is idle...");
            return;
        }

        Bundle results = new Bundle();

        TestExecutor.Builder executorBuilder = new TestExecutor.Builder(this);

        addListeners(mRunnerArgs, executorBuilder);

        TestRequest testRequest = buildRequest(mRunnerArgs, getArguments());

        extractFeatureAndExecute(results, executorBuilder, testRequest);

        super.finish(Activity.RESULT_OK, results);
    }

    @Override
    public void finish(int resultCode, Bundle results) {
        try {
            mUsageTrackerFacilitator.trackUsage("AndroidJUnitRunner");
            mUsageTrackerFacilitator.sendUsages();
        } catch (RuntimeException re) {
            Log.w(LOG_TAG, "Failed to send analytics.", re);
        }
    }

    @Override
    public boolean onException(Object obj, Throwable e) {
        InstrumentationResultPrinter instResultPrinter = getInstrumentationResultPrinter();
        if (instResultPrinter != null) {
            // report better error message back to Instrumentation results.
            instResultPrinter.reportProcessCrash(e);
        }
        return super.onException(obj, e);
    }

    private void extractFeatureAndExecute(Bundle results, TestExecutor.Builder executorBuilder,
                                          TestRequest testRequest) {
        try {
            List<Runner> runners = getTestRunners(testRequest, results);
            for (Runner runner : runners) {
                Class testClass = ((AndroidJUnit4) runner).getTestClass().getJavaClass();
                executeFeature(executorBuilder, testClass);
            }
        } catch (RuntimeException | IOException | NoSuchFieldException e) {
            logException(results, e);
        } catch (IllegalAccessException e) {
            logException(results, e);
        }
    }

    private void executeFeature(TestExecutor.Builder executorBuilder, Class testClass)
            throws IOException, IllegalAccessException, NoSuchFieldException {
        mWrapper = extractAnnotations(testClass);
        Feature feature = readFeature().getFeature();
        for (ScenarioDefinition scenario : feature.getChildren()) {
            injectScenarioValues(testClass, scenario);
            executeScenario(executorBuilder, testClass, scenario);
        }
    }

    private void executeScenario(TestExecutor.Builder executorBuilder, Class testClass,
                                 ScenarioDefinition scenario)
            throws IllegalAccessException, NoSuchFieldException {
        if (scenario instanceof ScenarioOutline) {
            List<Examples> examplesList = ((ScenarioOutline) scenario).getExamples();
            for (Examples examples : examplesList) {
                List<TableRow> tableRows = examples.getTableBody();
                for (TableRow tableRow : tableRows) {
                    injectExamplesValues(testClass, examples, tableRows.indexOf(tableRow));
                    executeTest(executorBuilder, testClass);
                }
            }
        } else {
            executeTest(executorBuilder, testClass);
        }
    }

    private void executeTest(TestExecutor.Builder executorBuilder, Class testClass) {
        String isDebug = mArguments.getString(DEBUG);
        Bundle featureArguments = new Bundle();
        featureArguments.putString(CLASS, testClass.getName());
        featureArguments.putString(DEBUG, isDebug);

        RunnerArgs featureRunnerArgs = new RunnerArgs.Builder()
                .fromManifest(this)
                .fromBundle(featureArguments)
                .build();

        Bundle results = executorBuilder.build().execute(
                buildFeatureRequest(featureRunnerArgs, featureArguments));
        mInstancesMap.clear();
        finish(Activity.RESULT_OK, results);
    }

    private void injectScenarioValues(@NonNull Class<?> testClass,
                                      @NonNull ScenarioDefinition scenario)
            throws IllegalAccessException, NoSuchFieldException {
        testClass.getField(InjectionFields.SCENARIO).set(null, scenario);
        testClass.getField(InjectionFields.WRAPPER).set(null, mWrapper);
        testClass.getField(InjectionFields.INSTANCES_MAP).set(null, mInstancesMap);
    }

    private void injectExamplesValues(@NonNull Class<?> testClass,
                                      @NonNull Examples examples,
                                      int rowNumber)
            throws IllegalAccessException, NoSuchFieldException {

        testClass.getField(InjectionFields.TABLE_HEADER).set(null, examples.getTableHeader());
        testClass.getField(InjectionFields.TABLE_ROW).set(null, examples.getTableBody().get(rowNumber));
    }

    /**
     * Build the arguments. Read from manifest first so manifest-provided args can be overridden
     * with command line arguments
     *
     * @param arguments
     */
    private void parseRunnerArgs(Bundle arguments) {
        mRunnerArgs = new RunnerArgs.Builder()
                .fromManifest(this)
                .fromBundle(arguments)
                .build();
    }

    /**
     * Get the Bundle object that contains the arguments passed to the instrumentation
     *
     * @return the Bundle object
     */
    private Bundle getArguments() {
        return mArguments;
    }

    @VisibleForTesting
    InstrumentationResultPrinter getInstrumentationResultPrinter() {
        return mInstrumentationResultPrinter;
    }

    private void addListeners(RunnerArgs args, TestExecutor.Builder builder) {
        if (args.suiteAssignment) {
            builder.addRunListener(new SuiteAssignmentPrinter());
        } else {
            builder.addRunListener(new LogRunListener());
            mInstrumentationResultPrinter = new InstrumentationResultPrinter();
            builder.addRunListener(mInstrumentationResultPrinter);
            builder.addRunListener(new ActivityFinisherRunListener(this,
                    new MonitoringInstrumentation.ActivityFinisher()));
            addDelayListener(args, builder);
            addCoverageListener(args, builder);
        }

        addListenersFromArg(args, builder);
    }

    private void addCoverageListener(RunnerArgs args, TestExecutor.Builder builder) {
        if (args.codeCoverage) {
            builder.addRunListener(new CoverageListener(args.codeCoveragePath));
        }
    }

    /**
     * Sets up listener to inject a delay between each test, if specified.
     */
    private void addDelayListener(RunnerArgs args, TestExecutor.Builder builder) {
        if (args.delayInMillis > 0) {
            builder.addRunListener(new DelayInjector(args.delayInMillis));
        } else if (args.logOnly && Build.VERSION.SDK_INT < 16) {
            // On older platforms, collecting tests can fail for large volume of tests.
            // Insert a small delay between each test to prevent this
            builder.addRunListener(new DelayInjector(15 /* msec */));
        }
    }

    private void addListenersFromArg(RunnerArgs args, TestExecutor.Builder builder) {
        for (RunListener listener : args.listeners) {
            builder.addRunListener(listener);
        }
    }

    /**
     * Builds a {@link TestRequest} based on given input arguments.
     * <p/>
     */
    @VisibleForTesting
    TestRequest buildRequest(RunnerArgs runnerArgs, Bundle bundleArgs) {

        TestRequestBuilder builder = createTestRequestBuilder(this, bundleArgs);

        // only scan for tests for current apk aka testContext
        // Note that this represents a change from InstrumentationTestRunner where
        // getTargetContext().getPackageCodePath() aka app under test was also scanned
        builder.addApkToScan(getContext().getPackageCodePath());

        builder.addFromRunnerArgs(runnerArgs);

        registerUserTracker();

        return builder.build();
    }

    /**
     * Builds a {@link TestRequest} based on given input arguments.
     * <p/>
     */
    @VisibleForTesting
    TestRequest buildFeatureRequest(RunnerArgs runnerArgs, Bundle bundleArgs) {

        FeatureTestRequestBuilder builder = createFeatureTestRequestBuilder(this, bundleArgs);

        // only scan for tests for current apk aka testContext
        // Note that this represents a change from InstrumentationTestRunner where
        // getTargetContext().getPackageCodePath() aka app under test was also scanned
        builder.addApkToScan(getContext().getPackageCodePath());

        builder.addFromRunnerArgs(runnerArgs);

        registerUserTracker();

        return builder.build();
    }

    private void registerUserTracker() {
        Context targetContext = getTargetContext();
        if (targetContext != null) {
            mUsageTrackerFacilitator.registerUsageTracker(new AnalyticsBasedUsageTracker.Builder(
                    targetContext).buildIfPossible());
        }
    }

    @NonNull
    private GherkinDocument readFeature() throws IOException {
        AssetManager assetManager = InstrumentationRegistry.getContext().getAssets();
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(assetManager.open(mWrapper.getFeature())));
        Parser<GherkinDocument> parser = new Parser<>(new AstBuilder());
        GherkinDocument document = parser.parse(reader);
        reader.close();
        return document;
    }

    @NonNull
    private List<Runner> getTestRunners(TestRequest testRequest, Bundle results) {
        List<Runner> runners;
        Runner runner = testRequest.getRequest().getRunner();
        runners = extractRunners(runner, results);
        if (!runners.isEmpty() && runners.get(0) instanceof Suite) {
            runners = extractRunners(runners.get(0), results);
        }
        return runners;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    private List<Runner> extractRunners(Runner runner, Bundle results) {
        List<Runner> runners = new ArrayList<>();
        try {
            Field field = runner.getClass().getDeclaredField(RUNNERS);
            field.setAccessible(true);
            runners = ((List<Runner>) field.get(runner));
        } catch (NoSuchFieldException e) {
            logException(results, e);
        } catch (IllegalAccessException e) {
            logException(results, e);
        }
        return runners;
    }

    private void logException(Bundle results, Exception e) {
        final String msg = "Fatal exception when running tests";
        Log.e(LOG_TAG, msg, e);
        // report the exception to instrumentation out
        results.putString(Instrumentation.REPORT_KEY_STREAMRESULT,
                msg + "\n" + Log.getStackTraceString(e));
        mInstrumentationResultPrinter.reportProcessCrash(e);
    }

    /**
     * Factory method for {@link TestRequestBuilder}.
     */
    @VisibleForTesting
    TestRequestBuilder createTestRequestBuilder(Instrumentation instr, Bundle arguments) {
        return new TestRequestBuilder(instr, arguments);
    }

    /**
     * Factory method for {@link FeatureTestRequestBuilder}.
     */
    @VisibleForTesting
    FeatureTestRequestBuilder createFeatureTestRequestBuilder(Instrumentation instr, Bundle
            arguments) {
        return new FeatureTestRequestBuilder(instr, arguments);
    }
}