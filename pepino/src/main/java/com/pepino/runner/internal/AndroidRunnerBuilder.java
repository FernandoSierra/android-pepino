package com.pepino.runner.internal;

import android.support.test.internal.runner.junit3.AndroidJUnit3Builder;
import android.support.test.internal.runner.junit3.AndroidSuiteBuilder;
import android.support.test.internal.runner.junit4.AndroidJUnit4Builder;
import android.support.test.internal.util.AndroidRunnerParams;

import org.junit.internal.builders.AllDefaultPossibilitiesBuilder;
import org.junit.internal.builders.AnnotatedBuilder;
import org.junit.internal.builders.IgnoredBuilder;
import org.junit.internal.builders.JUnit3Builder;
import org.junit.internal.builders.JUnit4Builder;
import org.junit.runners.model.RunnerBuilder;

/**
 * Extension of the {@link android.support.test.internal.runner.AndroidRunnerBuilder} to use
 * {@link CustomAndroidAnnotatedBuilder} instead of
 * {@link android.support.test.internal.runner.junit4.AndroidAnnotatedBuilder} and overwrites
 * the children description.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @see android.support.test.internal.runner.AndroidRunnerBuilder
 * @since 13/10/16
 */
class AndroidRunnerBuilder extends AllDefaultPossibilitiesBuilder {

    private final AndroidJUnit3Builder mAndroidJUnit3Builder;
    private final AndroidJUnit4Builder mAndroidJUnit4Builder;
    private final AndroidSuiteBuilder mAndroidSuiteBuilder;
    private final CustomAndroidAnnotatedBuilder mAndroidAnnotatedBuilder;
    // TODO: customize for Android ?
    private final IgnoredBuilder mIgnoredBuilder;

    /**
     * @param runnerParams {@link AndroidRunnerParams} that stores common runner parameters
     */
    public AndroidRunnerBuilder(AndroidRunnerParams runnerParams) {
        super(true);
        mAndroidJUnit3Builder = new AndroidJUnit3Builder(runnerParams);
        mAndroidJUnit4Builder = new AndroidJUnit4Builder(runnerParams);
        mAndroidSuiteBuilder = new AndroidSuiteBuilder(runnerParams);
        mAndroidAnnotatedBuilder = new CustomAndroidAnnotatedBuilder(this, runnerParams);
        mIgnoredBuilder = new IgnoredBuilder();
    }

    @Override
    protected JUnit4Builder junit4Builder() {
        return mAndroidJUnit4Builder;
    }

    @Override
    protected JUnit3Builder junit3Builder() {
        return mAndroidJUnit3Builder;
    }

    @Override
    protected AnnotatedBuilder annotatedBuilder() {
        return mAndroidAnnotatedBuilder;
    }

    @Override
    protected IgnoredBuilder ignoredBuilder() {
        return mIgnoredBuilder;
    }

    @Override
    protected RunnerBuilder suiteMethodBuilder() {
        return mAndroidSuiteBuilder;
    }
}