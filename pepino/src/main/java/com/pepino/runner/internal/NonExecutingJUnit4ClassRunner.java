package com.pepino.runner.internal;

import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

/**
 * @see android.support.test.internal.runner.junit4.NonExecutingJUnit4ClassRunner
 */
class NonExecutingJUnit4ClassRunner extends BlockJUnit4ClassRunner {

    private static final Statement NON_EXECUTING_STATEMENT = new Statement() {
        @Override
        public void evaluate() throws Throwable {
            // do nothing
        }
    };

    public NonExecutingJUnit4ClassRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    /**
     * Override parent to generate an non executing statement
     */
    @Override
    protected Statement methodBlock(FrameworkMethod method) {
        return NON_EXECUTING_STATEMENT;
    }
}