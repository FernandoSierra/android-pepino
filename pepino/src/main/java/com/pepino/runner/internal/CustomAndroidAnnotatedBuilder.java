package com.pepino.runner.internal;

import android.support.annotation.VisibleForTesting;
import android.support.test.internal.runner.junit3.JUnit38ClassRunner;
import android.support.test.internal.runner.junit3.NonExecutingTestSuite;
import android.support.test.internal.util.AndroidRunnerParams;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.internal.builders.AnnotatedBuilder;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.junit.runners.model.RunnerBuilder;

import static android.support.test.internal.util.AndroidRunnerBuilderUtil.isJUnit3Test;

/**
 * Extension of {@link android.support.test.internal.runner.junit4.AndroidAnnotatedBuilder} to
 * creates a new {@link FeatureRunner} instead of the one created by reflection inside the
 * {@link #buildAndroidRunner(Class)} method. That let us overwrites the children' description.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @see android.support.test.internal.runner.junit4.AndroidAnnotatedBuilder
 * @since 13/10/16
 */
public class CustomAndroidAnnotatedBuilder extends AnnotatedBuilder {
    private static final String LOG_TAG = "AndroidAnnotatedBuilder";

    private final AndroidRunnerParams mAndroidRunnerParams;

    public CustomAndroidAnnotatedBuilder(RunnerBuilder suiteBuilder, AndroidRunnerParams runnerParams) {
        super(suiteBuilder);
        mAndroidRunnerParams = runnerParams;
    }

    @Override
    public Runner runnerForClass(Class<?> testClass) throws Exception {
        try {
            // check if we need to skip execution and return an appropriate non executing runner
            if (mAndroidRunnerParams.isSkipExecution()) {
                if (isJUnit3Test(testClass)) {
                    return new JUnit38ClassRunner(new NonExecutingTestSuite(testClass));
                }
                return new NonExecutingJUnit4ClassRunner(testClass);
            }

            RunWith annotation = testClass.getAnnotation(RunWith.class);
            // check if its an Android specific runner otherwise default to AnnotatedBuilder
            if (annotation != null && annotation.value().equals(AndroidJUnit4.class)) {
                Class<? extends Runner> runnerClass = annotation.value();
                try {
                    // try to build an AndroidJUnit4 runner
                    Runner runner = buildAndroidRunner(testClass);
                    if (runner != null) {
                        return runner;
                    }
                } catch (NoSuchMethodException e) {
                    // let the super class handle the error for us and throw an InitializationError
                    // exception.
                    return super.buildRunner(runnerClass, testClass);
                }
            }
        } catch (Throwable e) {
            // log error message including stack trace before throwing to help with debugging.
            Log.e(LOG_TAG, "Error constructing runner", e);
            throw e;
        }
        return super.runnerForClass(testClass);
    }

    @VisibleForTesting
    public Runner buildAndroidRunner(Class<?> testClass) throws Exception {
        // Forces to create a custom FeatureRunner to modify the children' description.
        return new FeatureRunner(testClass, mAndroidRunnerParams);
    }
}