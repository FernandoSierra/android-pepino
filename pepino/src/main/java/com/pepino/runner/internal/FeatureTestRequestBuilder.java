package com.pepino.runner.internal;

import android.app.Instrumentation;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.test.filters.RequiresDevice;
import android.support.test.filters.SdkSuppress;
import android.support.test.internal.runner.RunnerArgs;
import android.support.test.internal.runner.TestLoader;
import android.support.test.internal.runner.TestRequest;
import android.support.test.internal.runner.TestSize;
import android.support.test.internal.util.AndroidRunnerParams;
import android.support.test.internal.util.Checks;
import android.util.Log;

import org.junit.runner.Computer;
import org.junit.runner.Description;
import org.junit.runner.Request;
import org.junit.runner.Runner;
import org.junit.runner.manipulation.Filter;
import org.junit.runner.manipulation.NoTestsRemainException;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Copy of {@link android.support.test.internal.runner.TestRequestBuilder} to extend and modify
 * the behavior when it creates the tests.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @see android.support.test.internal.runner.TestRequestBuilder
 * @since 13/10/16
 */
public class FeatureTestRequestBuilder {
    static final String EMULATOR_HARDWARE = "goldfish";
    static final String MISSING_ARGUMENTS_MSG =
            "Must provide either classes to run, or apks to scan";
    static final String AMBIGUOUS_ARGUMENTS_MSG =
            "Ambiguous arguments: cannot provide both test package and test class(es) to run";
    private static final String LOG_TAG = "TestRequestBuilder";
    // Excluded test packages
    private static final String[] DEFAULT_EXCLUDED_PACKAGES = {
            "junit",
            "org.junit",
            "org.hamcrest",
            "org.mockito",// exclude Mockito for performance and to prevent JVM related errors
            "android.support.test.internal.runner.junit3",// always skip AndroidTestSuite
            "org.jacoco"// exclude Jacoco to prevent class loading issues
    };
    private final FeatureTestRequestBuilder.DeviceBuild mDeviceBuild;
    private final Instrumentation mInstr;
    private final Bundle mArgsBundle;
    private List<String> mApkPaths = new ArrayList<>();
    private Set<String> mIncludedPackages = new HashSet<>();
    private Set<String> mExcludedPackages = new HashSet<>();
    private Set<String> mIncludedClasses = new HashSet<>();
    private Set<String> mExcludedClasses = new HashSet<>();
    private FeatureTestRequestBuilder.ClassAndMethodFilter mClassMethodFilter =
            new FeatureTestRequestBuilder.ClassAndMethodFilter();
    private Filter mFilter =
            new FeatureTestRequestBuilder.AnnotationExclusionFilter(android.support.test.filters
                    .Suppress.class)
                    .intersect(new FeatureTestRequestBuilder.AnnotationExclusionFilter(
                            android.test.suitebuilder.annotation.Suppress.class))
                    .intersect(new FeatureTestRequestBuilder.SdkSuppressFilter())
                    .intersect(new FeatureTestRequestBuilder.RequiresDeviceFilter())
                    .intersect(mClassMethodFilter);
    private boolean mSkipExecution = false;
    private long mPerTestTimeout = 0;
    private ClassLoader mClassLoader;

    /**
     * Instructs the test builder if JUnit3 suite() methods should be executed.
     * <p/>
     * Currently set to false if any method filter is set, for consistency with
     * InstrumentationTestRunner.
     */
    private boolean mIgnoreSuiteMethods = false;

    /**
     * Creates a FeatureTestRequestBuilder
     *
     * @param instr  the {@link Instrumentation} to pass to applicable tests
     * @param bundle the {@link Bundle} to pass to applicable tests
     */
    public FeatureTestRequestBuilder(Instrumentation instr, Bundle bundle) {
        this(new FeatureTestRequestBuilder.DeviceBuildImpl(), instr, bundle);
    }

    /**
     * Alternate FeatureTestRequestBuilder constructor that accepts a custom DeviceBuild
     */
    @VisibleForTesting
    FeatureTestRequestBuilder(FeatureTestRequestBuilder.DeviceBuild deviceBuildAccessor,
                              Instrumentation instr, Bundle bundle) {
        mDeviceBuild = Checks.checkNotNull(deviceBuildAccessor);
        mInstr = Checks.checkNotNull(instr);
        mArgsBundle = Checks.checkNotNull(bundle);
    }

    /**
     * Create a <code>Request</code> that, when processed, will run all the tests
     * in a set of classes.
     *
     * @param runnerParams {@link AndroidRunnerParams} that stores common runner parameters
     * @param computer     Helps construct Runners from classes
     * @param classes      the classes containing the tests
     * @return a <code>Request</code> that will cause all tests in the classes to be run
     */
    private static Request classes(AndroidRunnerParams runnerParams, Computer computer,
                                   Class<?>... classes) {
        try {
            Runner suite = computer.getSuite(new AndroidRunnerBuilder(runnerParams), classes);
            return Request.runner(suite);
        } catch (InitializationError e) {
            throw new RuntimeException(
                    "Suite constructor, called as above, should always complete");
        }
    }

    /**
     * Instruct builder to scan given apk, and add all tests classes found. Cannot be used in
     * conjunction with addTestClass or addTestMethod is used.
     *
     * @param apkPath
     */
    public FeatureTestRequestBuilder addApkToScan(String apkPath) {
        mApkPaths.add(apkPath);
        return this;
    }

    /**
     * Set the {@link ClassLoader} to be used to load test cases.
     *
     * @param loader {@link ClassLoader} to load test cases with.
     */
    public FeatureTestRequestBuilder setClassLoader(ClassLoader loader) {
        mClassLoader = loader;
        return this;
    }

    /**
     * Add a test class to be executed. All test methods in this class will be executed, unless a
     * test method was explicitly included or excluded.
     *
     * @param className
     */
    public FeatureTestRequestBuilder addTestClass(String className) {
        mIncludedClasses.add(className);
        return this;
    }

    /**
     * Excludes a test class. All test methods in this class will be excluded.
     *
     * @param className
     */
    public FeatureTestRequestBuilder removeTestClass(String className) {
        mExcludedClasses.add(className);
        return this;
    }

    /**
     * Adds a test method to run.
     */
    public FeatureTestRequestBuilder addTestMethod(String testClassName, String testMethodName) {
        mIncludedClasses.add(testClassName);
        mClassMethodFilter.addMethod(testClassName, testMethodName);
        mIgnoreSuiteMethods = true;
        return this;
    }

    /**
     * Excludes a test method from being run.
     */
    public FeatureTestRequestBuilder removeTestMethod(String testClassName, String testMethodName) {
        mClassMethodFilter.removeMethod(testClassName, testMethodName);
        mIgnoreSuiteMethods = true;
        return this;
    }

    /**
     * Run only tests within given java package. Cannot be used in conjunction with
     * addTestClass/Method.
     * <p/>
     * At least one addApkPath also must be provided.
     *
     * @param testPackage the fully qualified java package name
     */
    public FeatureTestRequestBuilder addTestPackage(String testPackage) {
        mIncludedPackages.add(testPackage);
        return this;
    }

    /**
     * Excludes all tests within given java package. Cannot be used in conjunction with
     * addTestClass/Method.
     * <p/>
     * At least one addApkPath also must be provided.
     *
     * @param testPackage the fully qualified java package name
     */
    public FeatureTestRequestBuilder removeTestPackage(String testPackage) {
        mExcludedPackages.add(testPackage);
        return this;
    }

    /**
     * Run only tests with given size
     *
     * @param forTestSize
     */
    public FeatureTestRequestBuilder addTestSizeFilter(TestSize forTestSize) {
        if (!TestSize.NONE.equals(forTestSize)) {
            mFilter = mFilter.intersect(
                    new FeatureTestRequestBuilder.SizeFilter(forTestSize));
        } else {
            Log.e(LOG_TAG, String.format("Unrecognized test size '%s'",
                    forTestSize.getSizeQualifierName()));
        }
        return this;
    }

    /**
     * Only run tests annotated with given annotation class.
     *
     * @param annotation the full class name of annotation
     */
    public FeatureTestRequestBuilder addAnnotationInclusionFilter(String annotation) {
        Class<? extends Annotation> annotationClass = loadAnnotationClass(annotation);
        if (annotationClass != null) {
            mFilter = mFilter.intersect(new FeatureTestRequestBuilder.AnnotationInclusionFilter(annotationClass));
        }
        return this;
    }

    /**
     * Skip tests annotated with given annotation class.
     *
     * @param notAnnotation the full class name of annotation
     */
    public FeatureTestRequestBuilder addAnnotationExclusionFilter(String notAnnotation) {
        Class<? extends Annotation> annotationClass = loadAnnotationClass(notAnnotation);
        if (annotationClass != null) {
            mFilter = mFilter.intersect(new FeatureTestRequestBuilder.AnnotationExclusionFilter(annotationClass));
        }
        return this;
    }

    public FeatureTestRequestBuilder addShardingFilter(int numShards, int shardIndex) {
        mFilter = mFilter.intersect(new FeatureTestRequestBuilder.ShardingFilter(numShards, shardIndex));
        return this;
    }

    /**
     * Build a request that will generate test started and test ended events, but will skip actual
     * test execution.
     */
    public FeatureTestRequestBuilder setSkipExecution(boolean b) {
        mSkipExecution = b;
        return this;
    }

    /**
     * Sets milliseconds timeout value applied to each test where 0 means no timeout
     */
    public FeatureTestRequestBuilder setPerTestTimeout(long millis) {
        mPerTestTimeout = millis;
        return this;
    }

    /**
     * Convenience method to set builder attributes from {@link RunnerArgs}
     */
    public FeatureTestRequestBuilder addFromRunnerArgs(RunnerArgs runnerArgs) {
        for (RunnerArgs.TestArg test : runnerArgs.tests) {
            if (test.methodName == null) {
                addTestClass(test.testClassName);
            } else {
                addTestMethod(test.testClassName, test.methodName);
            }
        }
        for (RunnerArgs.TestArg test : runnerArgs.notTests) {
            if (test.methodName == null) {
                removeTestClass(test.testClassName);
            } else {
                removeTestMethod(test.testClassName, test.methodName);
            }
        }
        for (String pkg : runnerArgs.testPackages) {
            addTestPackage(pkg);
        }
        for (String pkg : runnerArgs.notTestPackages) {
            removeTestPackage(pkg);
        }
        if (runnerArgs.testSize != null) {
            addTestSizeFilter(TestSize.fromString(runnerArgs.testSize));
        }
        if (runnerArgs.annotation != null) {
            addAnnotationInclusionFilter(runnerArgs.annotation);
        }
        for (String notAnnotation : runnerArgs.notAnnotations) {
            addAnnotationExclusionFilter(notAnnotation);
        }
        if (runnerArgs.testTimeout > 0) {
            setPerTestTimeout(runnerArgs.testTimeout);
        }
        if (runnerArgs.numShards > 0 && runnerArgs.shardIndex >= 0 &&
                runnerArgs.shardIndex < runnerArgs.numShards) {
            addShardingFilter(runnerArgs.numShards, runnerArgs.shardIndex);
        }
        if (runnerArgs.logOnly) {
            setSkipExecution(true);
        }
        return this;
    }

    /**
     * Builds the {@link TestRequest} based on provided data.
     *
     * @throws java.lang.IllegalArgumentException if provided set of data is not valid
     */
    public TestRequest build() {
        mIncludedPackages.removeAll(mExcludedPackages);
        mIncludedClasses.removeAll(mExcludedClasses);
        validate(mIncludedClasses);
        TestLoader loader = new TestLoader();
        loader.setClassLoader(mClassLoader);
        if (mIncludedClasses.isEmpty()) {
            // no class restrictions have been specified. Load all classes.
            loadClassesFromClassPath(loader, mExcludedClasses);
        } else {
            loadClasses(mIncludedClasses, loader);
        }

        Collection<Class<?>> loadedClasses = loader.getLoadedClasses();
        Request request = classes(new AndroidRunnerParams(mInstr, mArgsBundle, mSkipExecution,
                        mPerTestTimeout, mIgnoreSuiteMethods),
                new Computer(),
                loadedClasses.toArray(new Class[loadedClasses.size()]));
        return new TestRequest(loader.getLoadFailures(),
                new FeatureTestRequestBuilder.LenientFilterRequest(request, mFilter));
    }

    /**
     * Validate that the set of options provided to this builder are valid and not conflicting
     */
    private void validate(Set<String> classNames) {
        if (classNames.isEmpty() && mApkPaths.isEmpty()) {
            throw new IllegalArgumentException(MISSING_ARGUMENTS_MSG);
        }
        // TODO: consider failing if both test classes and apk paths are given.
        // Right now that is allowed though

        if ((!mIncludedPackages.isEmpty() || !mExcludedPackages.isEmpty())
                && !classNames.isEmpty()) {
            throw new IllegalArgumentException(AMBIGUOUS_ARGUMENTS_MSG);
        }
    }

    private void loadClassesFromClassPath(TestLoader loader, Set<String> excludedClasses) {
        Collection<String> classNames = getClassNamesFromClassPath();
        for (String className : classNames) {
            if (!excludedClasses.contains(className)) {
                loader.loadIfTest(className);
            }
        }
    }

    private void loadClasses(Collection<String> classNames, TestLoader loader) {
        for (String className : classNames) {
            loader.loadClass(className);
        }
    }

    private Collection<String> getClassNamesFromClassPath() {
        if (mApkPaths.isEmpty()) {
            throw new IllegalStateException("neither test class to execute or apk paths were provided");
        }
        Log.i(LOG_TAG, String.format("Scanning classpath to find tests in apks %s",
                mApkPaths));
        ClassPathScanner scanner = createClassPathScanner(mApkPaths);

        ClassPathScanner.ChainedClassNameFilter filter = new ClassPathScanner.ChainedClassNameFilter();
        // exclude inner classes
        filter.add(new ClassPathScanner.ExternalClassNameFilter());
        for (String pkg : DEFAULT_EXCLUDED_PACKAGES) {
            // Add the test packages to the exclude list unless they were explictly included.
            if (!mIncludedPackages.contains(pkg)) {
                mExcludedPackages.add(pkg);
            }
        }
        for (String pkg : mIncludedPackages) {
            filter.add(new ClassPathScanner.InclusivePackageNameFilter(pkg));
        }
        for (String pkg : mExcludedPackages) {
            filter.add(new ClassPathScanner.ExcludePackageNameFilter(pkg));
        }
        try {
            return scanner.getClassPathEntries(filter);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Failed to scan classes", e);
        }
        return Collections.emptyList();
    }

    /**
     * Factory method for {@link ClassPathScanner}.
     * <p/>
     * Exposed so unit tests can mock.
     */
    ClassPathScanner createClassPathScanner(List<String> apkPaths) {
        return new ClassPathScanner(apkPaths);
    }

    @SuppressWarnings("unchecked")
    private Class<? extends Annotation> loadAnnotationClass(String className) {
        try {
            Class<?> clazz = Class.forName(className);
            return (Class<? extends Annotation>) clazz;
        } catch (ClassNotFoundException e) {
            Log.e(LOG_TAG, String.format("Could not find annotation class: %s", className));
        } catch (ClassCastException e) {
            Log.e(LOG_TAG, String.format("Class %s is not an annotation", className));
        }
        return null;
    }

    private int getDeviceSdkInt() {
        return mDeviceBuild.getSdkVersionInt();
    }

    private String getDeviceHardware() {
        return mDeviceBuild.getHardware();
    }

    /**
     * Accessor interface for retrieving device build properties.
     * <p/>
     * Used so unit tests can mock calls
     */
    interface DeviceBuild {
        /**
         * Returns the SDK API level for current device.
         */
        int getSdkVersionInt();

        /**
         * Returns the hardware type of the current device.
         */
        String getHardware();
    }

    private static class DeviceBuildImpl implements FeatureTestRequestBuilder.DeviceBuild {
        @Override
        public int getSdkVersionInt() {
            return android.os.Build.VERSION.SDK_INT;
        }

        @Override
        public String getHardware() {
            return android.os.Build.HARDWARE;
        }
    }

    /**
     * Helper parent class for {@link Filter} that allows suites to run if any child matches.
     */
    private abstract static class ParentFilter extends Filter {
        /**
         * {@inheritDoc}
         */
        @Override
        public boolean shouldRun(Description description) {
            if (description.isTest()) {
                return evaluateTest(description);
            }
            // this is a suite, explicitly check if any children should run
            for (Description each : description.getChildren()) {
                if (shouldRun(each)) {
                    return true;
                }
            }
            // no children to run, filter this out
            return false;
        }

        /**
         * Determine if given test description matches filter.
         *
         * @param description the {@link Description} describing the test
         * @return <code>true</code> if matched
         */
        protected abstract boolean evaluateTest(Description description);
    }

    /**
     * Filter that only runs tests whose method or class has been annotated with given filter.
     */
    private static class AnnotationInclusionFilter extends FeatureTestRequestBuilder.ParentFilter {

        private final Class<? extends Annotation> mAnnotationClass;

        AnnotationInclusionFilter(Class<? extends Annotation> annotation) {
            mAnnotationClass = annotation;
        }

        /**
         * Determine if given test description matches filter.
         *
         * @param description the {@link Description} describing the test
         * @return <code>true</code> if matched
         */
        @Override
        protected boolean evaluateTest(Description description) {
            final Class<?> testClass = description.getTestClass();
            return description.getAnnotation(mAnnotationClass) != null ||
                    (testClass != null && testClass.isAnnotationPresent(mAnnotationClass));
        }

        protected Class<? extends Annotation> getAnnotationClass() {
            return mAnnotationClass;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String describe() {
            return String.format("annotation %s", mAnnotationClass.getName());
        }
    }

    /**
     * A filter for test sizes.
     * <p/>
     * Will match if test method has given size annotation, or class does, but only if method does
     * not have any other size annotations. ie method size annotation overrides class size
     * annotation.
     */
    private static class SizeFilter extends FeatureTestRequestBuilder.ParentFilter {

        private final TestSize mTestSize;

        SizeFilter(TestSize testSize) {
            mTestSize = testSize;
        }

        @Override
        public String describe() {
            return "";
        }

        @Override
        protected boolean evaluateTest(Description description) {
            // If test method is annotated with test size annotation include it
            if (mTestSize.testMethodIsAnnotatedWithTestSize(description)) {
                return true;
            } else if (mTestSize.testClassIsAnnotatedWithTestSize(description)) {
                // size annotation matched at class level. Make sure method doesn't have any other
                // size annotations
                for (Annotation a : description.getAnnotations()) {
                    if (TestSize.isAnyTestSize(a.annotationType())) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }

    /**
     * Filter out tests whose method or class has been annotated with given filter.
     */
    private static class AnnotationExclusionFilter extends FeatureTestRequestBuilder.ParentFilter {

        private final Class<? extends Annotation> mAnnotationClass;

        AnnotationExclusionFilter(Class<? extends Annotation> annotation) {
            mAnnotationClass = annotation;
        }

        @Override
        protected boolean evaluateTest(Description description) {
            final Class<?> testClass = description.getTestClass();
            if ((testClass != null && testClass.isAnnotationPresent(mAnnotationClass))
                    || (description.getAnnotation(mAnnotationClass) != null)) {
                return false;
            }
            return true;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String describe() {
            return String.format("not annotation %s", mAnnotationClass.getName());
        }
    }

    private static class ShardingFilter extends Filter {
        private final int mNumShards;
        private final int mShardIndex;

        ShardingFilter(int numShards, int shardIndex) {
            mNumShards = numShards;
            mShardIndex = shardIndex;
        }

        @Override
        public boolean shouldRun(Description description) {
            if (description.isTest()) {
                return (Math.abs(description.hashCode()) % mNumShards) == mShardIndex;
            }
            // this is a suite, explicitly check if any children should run
            for (Description each : description.getChildren()) {
                if (shouldRun(each)) {
                    return true;
                }
            }
            // no children to run, filter this out
            return false;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String describe() {
            return String.format("Shard %s of %s shards", mShardIndex, mNumShards);
        }
    }

    /**
     * A {@link Request} that doesn't report an error if all tests are filtered out. Done for
     * consistency with InstrumentationTestRunner.
     */
    private static class LenientFilterRequest extends Request {
        private final Request mRequest;
        private final Filter mFilter;

        public LenientFilterRequest(Request classRequest, Filter filter) {
            mRequest = classRequest;
            mFilter = filter;
        }

        @Override
        public Runner getRunner() {
            try {
                Runner runner = mRequest.getRunner();
                mFilter.apply(runner);
                return runner;
            } catch (NoTestsRemainException e) {
                // don't treat filtering out all tests as an error
                return new FeatureTestRequestBuilder.BlankRunner();
            }
        }
    }

    /**
     * A {@link Runner} that doesn't do anything
     */
    private static class BlankRunner extends Runner {
        @Override
        public Description getDescription() {
            return Description.createSuiteDescription("no tests found");
        }

        @Override
        public void run(RunNotifier notifier) {
            // do nothing
        }
    }

    /**
     * A {@link Filter} to support the ability to filter out multiple class#method combinations.
     */
    private static class ClassAndMethodFilter extends Filter {

        private Map<String, FeatureTestRequestBuilder.MethodFilter> mMethodFilters = new HashMap<>();

        public void addMethod(String className, String methodName) {
            FeatureTestRequestBuilder.MethodFilter mf = mMethodFilters.get(className);
            if (mf == null) {
                mf = new FeatureTestRequestBuilder.MethodFilter(className);
                mMethodFilters.put(className, mf);
            }
            mf.add(methodName);
        }

        public void removeMethod(String className, String methodName) {
            FeatureTestRequestBuilder.MethodFilter mf = mMethodFilters.get(className);
            if (mf == null) {
                mf = new FeatureTestRequestBuilder.MethodFilter(className);
                mMethodFilters.put(className, mf);
            }
            mf.remove(methodName);
        }

        @Override
        public boolean shouldRun(Description description) {
            if (mMethodFilters.isEmpty()) {
                return true;
            }
            if (description.isTest()) {
                String className = description.getClassName();
                FeatureTestRequestBuilder.MethodFilter methodFilter = mMethodFilters.get(className);
                if (methodFilter != null) {
                    return methodFilter.shouldRun(description);
                }
            } else {
                // Check all children, if any
                for (Description child : description.getChildren()) {
                    if (shouldRun(child)) {
                        return true;
                    }
                }
            }
            return false;
        }


        @Override
        public String describe() {
            return "Class and method filter";
        }


    }

    /**
     * A {@link Filter} used to filter out desired test methods from a given class
     */
    private static class MethodFilter extends Filter {

        private final String mClassName;
        private Set<String> mIncludedMethods = new HashSet<>();
        private Set<String> mExcludedMethods = new HashSet<>();

        /**
         * Constructs a method filter for a given class
         *
         * @param className name of the class the method belongs to
         */
        public MethodFilter(String className) {
            mClassName = className;
        }

        // Strips out the parameterized suffix if it exists
        private String stripParameterizedSuffix(String name) {
            Pattern suffixPattern = Pattern.compile(".+(\\[[0-9]+\\])$");
            if (suffixPattern.matcher(name).matches()) {
                name = name.substring(0, name.lastIndexOf('['));
            }
            return name;
        }

        public void add(String methodName) {
            mIncludedMethods.add(methodName);
        }

        public void remove(String methodName) {
            mExcludedMethods.add(methodName);
        }

        @Override
        public String describe() {
            return "Method filter for " + mClassName + " class";
        }


        @Override
        public boolean shouldRun(Description description) {
            if (description.isTest()) {
                String methodName = description.getMethodName();
                // Parameterized tests append "[#]" at the end of the method names.
                // For instance, "getFoo" would become "getFoo[0]".
                methodName = stripParameterizedSuffix(methodName);
                if (mExcludedMethods.contains(methodName)) {
                    return false;
                }
                // don't filter out descriptions with method name "initializationError", since
                // Junit will generate such descriptions in error cases, See ErrorReportingRunner
                return mIncludedMethods.isEmpty() || mIncludedMethods.contains(methodName)
                        || methodName.equals("initializationError");
            }
            // At this point, this could only be a description of this filter
            return true;

        }


    }

    private class SdkSuppressFilter extends FeatureTestRequestBuilder.ParentFilter {

        private SdkSuppress getAnnotationForTest(Description description) {
            final SdkSuppress s = description.getAnnotation(SdkSuppress.class);
            if (s != null) {
                return s;
            }
            final Class<?> testClass = description.getTestClass();
            if (testClass != null) {
                return testClass.getAnnotation(SdkSuppress.class);
            }
            return null;
        }

        @Override
        protected boolean evaluateTest(Description description) {
            final SdkSuppress s = getAnnotationForTest(description);
            if (s != null && getDeviceSdkInt() < s.minSdkVersion()) {
                return false;
            }
            return true;
        }


        /**
         * {@inheritDoc}
         */
        @Override
        public String describe() {
            return String.format("skip tests annotated with SdkSuppress if necessary");
        }
    }

    /**
     * Class that filters out tests annotated with {@link RequiresDevice} when running on emulator
     */
    private class RequiresDeviceFilter extends FeatureTestRequestBuilder.AnnotationExclusionFilter {

        RequiresDeviceFilter() {
            super(RequiresDevice.class);
        }

        @Override
        protected boolean evaluateTest(Description description) {
            if (!super.evaluateTest(description)) {
                // annotation is present - check if device is an emulator
                return !EMULATOR_HARDWARE.equals(getDeviceHardware());
            }
            return true;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String describe() {
            return String.format("skip tests annotated with RequiresDevice if necessary");
        }
    }
}
