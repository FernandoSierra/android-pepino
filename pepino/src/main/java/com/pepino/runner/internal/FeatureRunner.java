package com.pepino.runner.internal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import android.support.test.internal.util.AndroidRunnerParams;
import android.util.Log;

import com.pepino.runner.InjectionFields;

import org.junit.runner.Description;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

import gherkin.ast.ScenarioDefinition;
import gherkin.ast.ScenarioOutline;
import gherkin.ast.TableRow;

/**
 * Used to overwrite the methods' description inside the {@link #describeChild(FrameworkMethod)}
 * method.
 *
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @see AndroidJUnit4ClassRunner
 * @since 13/10/16
 */
public class FeatureRunner extends AndroidJUnit4ClassRunner {

    public FeatureRunner(Class<?> clazz, AndroidRunnerParams runnerParams)
            throws InitializationError {
        super(clazz, runnerParams);
    }

    @Override
    protected Description describeChild(FrameworkMethod method) {
        Class<?> javaClass = getTestClass().getJavaClass();
        return Description.createTestDescription(javaClass,
                getChildName(javaClass),
                method.getAnnotations());
    }

    @NonNull
    private String getChildName(Class javaClass) {
        String name = "Scenario: ";
        try {
            ScenarioDefinition scenario = ((ScenarioDefinition) javaClass
                    .getField(InjectionFields.SCENARIO)
                    .get(null));
            name += scenario.getName();
            if (scenario instanceof ScenarioOutline) {
                TableRow body = ((TableRow) javaClass
                        .getField(InjectionFields.TABLE_ROW)
                        .get(null));
                name += " (" + body.getCells().get(0).getValue() + ")";
                //name += getExamplesString(javaClass);
            }
        } catch (NoSuchFieldException e) {
            Log.e(getClass().getSimpleName(), e.getMessage(), e);
        } catch (IllegalAccessException e) {
            Log.e(getClass().getSimpleName(), e.getMessage(), e);
        }
        return name;
    }

    @Nullable
    private String getExamplesString(Class javaClass)
            throws IllegalAccessException, NoSuchFieldException {
        TableRow header = ((TableRow) javaClass
                .getField(InjectionFields.TABLE_HEADER)
                .get(null));
        TableRow body = ((TableRow) javaClass
                .getField(InjectionFields.TABLE_ROW)
                .get(null));
        String rowString = "{";
        for (int index = 0; index < header.getCells().size(); index++) {
            rowString += index > 0 ? ", " : "";
            rowString += header.getCells().get(index).getValue() +
                    "='" +
                    body.getCells().get(index).getValue() +
                    "'";
        }
        rowString += "}";
        return rowString;
    }
}
